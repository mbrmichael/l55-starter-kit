<?php

use App\Models\Setting;

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Setting::create([
            'key'         => 'site.title',
            'value'       => 'Laravel Starter Kit',
            'description' => 'Site title',
        ]);

        Setting::create([
            'key'         => 'site.description',
            'value'       => 'Just a Laravel Starter Kit',
            'description' => 'Site description for SEO',
        ]);

        Setting::create([
            'key'         => 'contact.address',
            'value'       => "Jl. Raya No.1, Jakarta Selatan, 123456",
            'description' => 'Address on contact us.',
        ]);

        Setting::create([
            'key'         => 'contact.email',
            'value'       => 'contact@example.com',
            'description' => 'Email for contact us.',
        ]);

        Setting::create([
            'key'         => 'social.youtube',
            'value'       => '',
            'description' => 'Youtube link of the site',
        ]);

        Setting::create([
            'key'         => 'social.facebook',
            'value'       => '',
            'description' => 'Facebook link of the site',
        ]);

        Setting::create([
            'key'         => 'social.twitter',
            'value'       => '',
            'description' => 'Twitter link of the site',
        ]);

        Setting::create([
            'key'         => 'social.instagram',
            'value'       => '',
            'description' => 'Instagram link of the site',
        ]);

    }
}
