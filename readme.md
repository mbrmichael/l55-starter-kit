# Laravel 5.5 Starter Kit

## Includes
- [ ] Sentinel
- [ ] Intervention Image
- [ ] Debugbar
- [ ] jQuery
- [ ] Bootstrap
- [ ] FontAwesome
- [ ] Admin page

## Changes & Structures
- Models moved to `app/Models/`.
- Apache & console log files placed separately.

## Getting Started
1. Run `composer install`
2. Copy example env file `cp .env.example .env`, then configure your database connection
3. Generate the App key `php artisan key:generate`
4. Add write permission to `storage` folder `chmod -R 777 storage/`
5. Run `php artisan app:install`
6. Run `npm install` or `yarn install`
