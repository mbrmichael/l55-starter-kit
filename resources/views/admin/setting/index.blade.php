@extends('admin.layouts.master')

@section('content')
  <div class="page-header">
    <h1 class="page-title">Settings</h1>
    <a href="{{ route('admin.setting.create') }}" class="btn btn-secondary btn-sm ml-5">
      <i class="fe fe-plus fa-fw"></i> Add New
    </a>
  </div>

  @include('admin._partials.notifications')

  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <div class="card-options">
            <button class="btn btn-secondary" data-toggle="modal" data-target="#cachedModal">View Cached</button>
          </div>
        </div>
        <div class="table-responsive">
          <table class="table card-table table-vcenter table-hover">
            <thead>
              <tr>
                <th class="w-1">No.</th>
                <th>Key</th>
                <th>Value</th>
                <th>Description</th>
                <th>
                  <span class="sr-only">Actions</span>
                </th>
              </tr>
            </thead>
            <tbody>
              @foreach ($settings as $key => $setting)
                <tr>
                  <td><span class="text-muted">{{ $settings->firstItem() + $key }}</span></td>
                  <td>{{ $setting->key }}</td>
                  <td title="{{ $setting->value }}">{!! str_limit(nl2br(e($setting->value)), 100) !!}</td>
                  <td>{{ $setting->description }}</td>
                  <td class="text-right">
                    <div class="dropdown">
                      <button class="btn btn-secondary btn-sm dropdown-toggle" data-toggle="dropdown">Actions</button>
                      <div class="dropdown-menu dropdown-menu-right">
                        <a href="{{ route('admin.setting.edit', $setting->id) }}" class="dropdown-item"><i class="dropdown-icon fe fe-edit"></i> Edit </a>
                      </div>
                    </div>
                  </td>
                </tr>
              @endforeach

              @if ($settings->count() < 1)
                <tr><td colspan="8">No records yet.</td></tr>
              @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

  <!-- Cached Modal -->
  <div class="modal fade" id="cachedModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Cached Settings</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"></span>
          </button>
        </div>
        <div class="modal-body">
          <table class="table table-bordered table-hover table-sm">
            <thead>
              <tr>
                <th>Key</th>
                <th>Value</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($cached as $c)
                <tr>
                  <td>{{ $c->key }}</td>
                  <td title="{{ $c->value }}">{{ str_limit($c->value, 100) }}</td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <div class="modal-footer">
          <a href="{{ route('admin.setting.rebuildcache') }}" class="btn btn-warning">Rebuild</a>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  {{ $settings->links() }}
@endsection
