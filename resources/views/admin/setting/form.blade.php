@extends('admin.layouts.master')

@section('content')
  <div class="page-header">
    <h1 class="page-title">Setting</h1>
  </div>

  @include('admin._partials.notifications')

  <form id="edit-form" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}

    <div class="row">
      <div class="col-lg-9">
        <div class="card">
          <div class="card-body">
            <div class="form-group">
              <div class="row align-items-center">
                <label class="col-md-2 mt-2">Key</label>
                <div class="col-md-10">
                  @if ($setting->id)
                    <div class="form-control-plaintext">{{ $setting->key }}</div>
                  @else
                    <input type="text" name="key" class="form-control" value="{{ old('key', $setting->key) }}" placeholder="scope.key">
                  @endif
                </div>
              </div>
            </div>

            <div class="form-group">
              <div class="row">
                <label class="col-md-2 mt-2">Value</label>
                <div class="col-md-10">
                  <textarea name="value" class="form-control" rows="5">{{ old('value', $setting->value) }}</textarea>
                </div>
              </div>
            </div>

            <div class="form-group">
              <div class="row align-items-center">
                <label class="col-md-2 mt-2">Description</label>
                <div class="col-md-10">
                  @if ($setting->id)
                    <div class="form-control-plaintext">{{ $setting->description }}</div>
                  @else
                    <input type="text" name="description" class="form-control" value="{{ old('description', $setting->description) }}">
                  @endif
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="card">
          <div class="card-body">

            <div class="form-group">
              <button type="submit" class="btn btn-primary btn-block">Save</button>
              <a href="{{ route('admin.setting') }}" class="btn btn-secondary btn-block">Cancel</a>
            </div>

          </div>
        </div>
      </div>
    </div>
  </form>
@endsection
