@extends('admin.layouts.master')

@section('content')
  <div class="page-header">
    <h1 class="page-title">Change Password</h1>
  </div>

  @include('admin._partials.notifications')

  <form id="edit-form" method="post">
    {{ csrf_field() }}

    <div class="row">
      <div class="col-9">
        <div class="card">
          <div class="card-body">

            <div class="form-group">
              <div class="row">
                <label class="col-2 mt-2">New Password</label>
                <div class="col-5">
                  <input type="password" class="form-control" name="password" value="{{ old('password') }}">
                </div>
              </div>
            </div>

            <div class="form-group">
              <div class="row">
                <label class="col-2 mt-2">Confirm Password</label>
                <div class="col-5">
                  <input type="password" class="form-control" name="password_confirmation" value="{{ old('password') }}">
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
      <div class="col-3">
        <div class="card">
          <div class="card-body">

            <div class="form-group">
              <button type="submit" class="btn btn-primary btn-block">Save</button>
              <a href="{{ route('admin.dashboard') }}" class="btn btn-secondary btn-block">Cancel</a>
            </div>

          </div>
        </div>
      </div>
    </div>
  </form>
@endsection
