@extends('admin.layouts.simple')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col col-login mx-auto">
        {{-- <div class="text-center mb-6"> --}}
          {{-- <img src="{{ assets_url('admin/tabler/brand/tabler.svg') }}" class="h-6" alt=""> --}}
        {{-- </div> --}}
        <form class="card" method="post">
          {{ csrf_field() }}
          <div class="card-body p-6">
            <div class="card-title">Login to your account</div>

            @include('admin._partials.notifications')

            <div class="form-group">
              <label class="form-label">Email address</label>
              <input type="email" name="email" class="form-control" value="{{ old('email') }}" aria-describedby="emailHelp" placeholder="Enter email">
            </div>
            <div class="form-group">
              <label class="form-label">
                Password
                {{-- <a href="./forgot-password.html" class="pull-right small">I forgot password</a> --}}
              </label>
              <input type="password" name="password" class="form-control" placeholder="Password">
            </div>
            <div class="form-group">
              <label class="custom-control custom-checkbox">
                <input type="checkbox" name="remember" class="custom-control-input" value="1" {{ old('rememeber') == 1 ? 'checked' : '' }} />
                <span class="custom-control-label">Remember me</span>
              </label>
            </div>
            <div class="form-footer">
              <button type="submit" class="btn btn-primary btn-block">Sign in</button>
            </div>
          </div>
        </form>
        <div class="text-center text-muted">
          Not an Admin? <a href="{{ route('home') }}">Back to Homepage</a>
        </div>
      </div>
    </div>
  </div>
@endsection
