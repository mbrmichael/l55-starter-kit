@extends('admin.layouts.master')

@section('content')
  <div class="page-header">
    <h1 class="page-title">
      Dashboard
    </h1>
  </div>

  @include('admin._partials.notifications')

  <div class="row">
    @foreach ($totals as $label => $total)
      <div class="col-6 col-sm-4 col-lg-3">
        <div class="card">
          <div class="card-body p-3 text-center">
            <div class="text-right">
              &nbsp;
            </div>
            <div class="h1 m-0">{{ $total }}</div>
            <div class="text-muted mb-4">{{ $label }}</div>
          </div>
        </div>
      </div>
    @endforeach
  </div>
@endsection
