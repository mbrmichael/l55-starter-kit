<div class="header collapse d-lg-flex p-0" id="headerMenuCollapse">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-lg order-lg-first">
        <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
          <li class="nav-item">
            <a href="{{ route('admin.dashboard') }}" class="nav-link {{ request()->is('admin') ? 'active' : '' }}"><i class="fe fe-home"></i> Dashboard</a>
          </li>
          {{-- <li class="nav-item">
            <a href="{{ route('admin.post') }}" class="nav-link {{ request()->is('admin/post*') ? 'active' : '' }}"><i class="fa fa-newspaper-o"></i> News</a>
          </li> --}}
          {{-- <li class="nav-item">
            <a href="{{ route('admin.visit') }}" class="nav-link {{ request()->is('admin/visit*') ? 'active' : '' }}"><i class="fe fe-inbox"></i> Visitor</a>
          </li> --}}
          {{--
          <li class="nav-item dropdown">
            <a href="javascript:void(0)" class="nav-link {{ request()->is('admin/creator*') || request()->is('admin/video*') || request()->is('admin/article*') ? 'active' : '' }}" data-toggle="dropdown"><i class="fe fe-play-circle"></i> Creators</a>
            <div class="dropdown-menu dropdown-menu-arrow">
              <a href="{{ route('admin.video') }}" class="dropdown-item {{ request()->is('admin/video*') ? 'active' : '' }}">Videos</a>
              <a href="{{ route('admin.article') }}" class="dropdown-item {{ request()->is('admin/article*') ? 'active' : '' }}">Articles</a>
            </div>
          </li>
          --}}
          <li class="nav-item">
            <a href="{{ route('admin.setting') }}" class="nav-link {{ request()->is('admin/setting*') ? 'active' : '' }}"><i class="fa fa-gear"></i> Settings</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
