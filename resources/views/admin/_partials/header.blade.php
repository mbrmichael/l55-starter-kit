<div class="header py-4">
  <div class="container">
    <div class="d-flex">
      <a href="{{ route('admin.dashboard') }}" class="header-brand">
        {{-- <img src="./demo/brand/tabler.svg" class="navbar-brand-img" alt="tabler.io"> --}}
        Admin
      </a>
      <div class="d-flex order-lg-2 ml-auto">
        <a href="{{ route('home') }}" class="nav-link icon" target="_blank">
          <i class="fe fe-home"></i>
          {{-- <span class="nav-unread"></span> --}}
        </a>
        <div class="dropdown">
          <a href="#" class="nav-link pr-0 leading-none" data-toggle="dropdown">
            <span class="avatar" style="background-image: url({{ assets_url('admin/images/user.png') }})"></span>
            <span class="ml-2 d-none d-lg-block">
              <span class="text-default">{{ Auth::user()->name }}</span>
              <small class="text-muted d-block mt-1">Administrator</small>
            </span>
          </a>
          <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
            <a class="dropdown-item" href="{{ route('admin.change-password') }}">
              <span>Change Password</span>
            </a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="{{ route('admin.logout') }}">Sign out</a>
          </div>
        </div>
      </div>
      <a href="#" class="header-toggler d-lg-none ml-3 ml-lg-0" data-toggle="collapse" data-target="#headerMenuCollapse">
        <span class="header-toggler-icon"></span>
      </a>
    </div>
  </div>
</div>
