<!doctype html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Language" content="en" />
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="theme-color" content="#4188c9">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    {{-- <link rel="icon" href="./favicon.ico" type="image/x-icon"/> --}}
    {{-- <link rel="shortcut icon" type="image/x-icon" href="./favicon.ico" /> --}}
    <!-- Generated: 2018-03-23 13:46:25 +0100 -->
    <title>Admin - {{ config('site.title') }}</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=latin-ext">
    <script src="{{ assets_url('admin/tabler/js/require.min.js') }}"></script>
    <script>
      requirejs.config({
        baseUrl: '{{ route('home') }}'
      });
    </script>
    <!-- jQuery -->
    <script src="{{ assets_url('admin/tabler/js/vendors/jquery-3.2.1.min.js') }}"></script>
    <!-- Dashboard Core -->
    <link href="{{ assets_url('admin/tabler/css/dashboard.css') }}" rel="stylesheet" />
    <script src="{{ assets_url('admin/tabler/js/dashboard.js') }}"></script>
    <!-- c3.js Charts Plugin -->
    <link href="{{ assets_url('admin/tabler/plugins/charts-c3/plugin.css') }}" rel="stylesheet" />
    <script src="{{ assets_url('admin/tabler/plugins/charts-c3/plugin.js') }}"></script>
    <!-- Google Maps Plugin -->
    <link href="{{ assets_url('admin/tabler/plugins/maps-google/plugin.css') }}" rel="stylesheet" />
    <script src="{{ assets_url('admin/tabler/plugins/maps-google/plugin.js') }}"></script>
    <!-- Input Mask Plugin -->
    <script src="{{ assets_url('admin/tabler/plugins/input-mask/plugin.js') }}"></script>
  </head>
  <body class="">
    <div class="page">
      <div class="page-single">
        @yield('content')
      </div>
    </div>
  </body>
</html>
