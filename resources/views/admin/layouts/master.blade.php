<!doctype html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="icon" href="./favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" type="image/x-icon" href="./favicon.ico" />

    <title>Admin - {{ config('site.title') }}</title>

    <meta name="_token" content="{{ csrf_token() }}" />

    <!-- Media URLs -->
    <meta name="image_upload" content="{{ route('admin.imageupload') }}" />
    <meta name="image_list" content="{{ route('admin.imagelist') }}" />

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=latin-ext">

    <!-- Datepicker -->
    {{-- <link rel="stylesheet" href="{{ assets_url('admin/css/vendors/bootstrap-datepicker.min.css') }}"> --}}
    <!-- Redactor -->
    <link rel="stylesheet" href="{{ assets_url('admin/css/vendors/redactor/redactor.min.css') }}?v=1">
    <link rel="stylesheet" href="{{ assets_url('admin/css/admin.css') }}?v={{ filemtime(public_path() . '/assets/admin/css/admin.css') }}">

    <script src="{{ assets_url('admin/tabler/js/require.min.js') }}"></script>
    <script>
      requirejs.config({
        baseUrl: '{{ route('home') }}'
      });
    </script>
    <!-- jQuery -->
    <script src="{{ assets_url('admin/tabler/js/vendors/jquery-3.2.1.min.js') }}"></script>
    <!-- Dashboard Core -->
    <link href="{{ assets_url('admin/tabler/css/dashboard.css') }}" rel="stylesheet" />
    <script src="{{ assets_url('admin/tabler/js/dashboard.js') }}"></script>
    <!-- c3.js Charts Plugin -->
    <link href="{{ assets_url('admin/tabler/plugins/charts-c3/plugin.css') }}" rel="stylesheet" />
    <script src="{{ assets_url('admin/tabler/plugins/charts-c3/plugin.js') }}"></script>
    <!-- Google Maps Plugin -->
    <link href="{{ assets_url('admin/tabler/plugins/maps-google/plugin.css') }}" rel="stylesheet" />
    <script src="{{ assets_url('admin/tabler/plugins/maps-google/plugin.js') }}"></script>
    <!-- Input Mask Plugin -->
    <script src="{{ assets_url('admin/tabler/plugins/input-mask/plugin.js') }}"></script>
    <!-- Vue -->
    <script src="{{ assets_url('admin/tabler/plugins/vue/plugin.js') }}"></script>
    <!-- Datepicker -->
    {{-- <script src="{{ assets_url('admin/js/vendors/bootstrap-datepicker.min.js') }}"></script> --}}
    <!-- Redactor -->
    <script src="{{ assets_url('admin/js/vendors/redactor/redactor.min.js') }}?v=1"></script>
    <script src="{{ assets_url('admin/js/vendors/redactor/video.js') }}"></script>
    <script src="{{ assets_url('admin/js/vendors/redactor/alignment.js') }}"></script>
    <script src="{{ assets_url('admin/js/vendors/redactor/table.js') }}"></script>
    <script src="{{ assets_url('admin/js/vendors/redactor/imagemanager.js') }}"></script>

    <script src="{{ assets_url('admin/js/admin.js') }}?v={{ filemtime(public_path() . '/assets/admin/js/admin.js') }}"></script>

    @yield('styles')
  </head>
  <body class="">
    <div class="page">
      <div class="page-main">

        @include('admin._partials.header')

        @include('admin._partials.navigation')

        <div class="page-content">
          <div class="container">

            @yield('content')

          </div>
        </div>
      </div>

      @include('admin._partials.footer')

      @yield('scripts')
    </div>
  </body>
</html>
