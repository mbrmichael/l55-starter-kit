@if ($message = session()->get('success'))
  <div class="alert alert-success alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert"></button>
    {{ $message }} <br>
  </div>
@endif

@if ($message = session()->get('info'))
  <div class="alert alert-info alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert"></button>
    {{ $message }} <br>
  </div>
@endif

@if ($message = session()->get('error'))
  <div class="alert alert-danger alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert"></button>
    {{ $message }} <br>
  </div>
@endif

@if (count($errors->all()) > 0)
  <div class="alert alert-danger alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert"></button>
    @foreach ($errors->all() as $key => $message)
      @if ($key > 0)
        <br>
      @endif
      {{ $message }}
    @endforeach
  </div>
@endif
