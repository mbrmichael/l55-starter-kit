<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="{{ assets_url('js/vendors/jquery-3.3.1.min.js') }}"><\/script>')</script>
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/js/bootstrap.min.js"></script> --}}
{{-- <script src="{{ assets_url('js/vendors/bootstrap.min.js') }}"></script> --}}
<script src="{{ assets_url('js/vendors/bootstrap.bundle.min.js') }}"></script>
<script src="{{ assets_url('js/site.js') }}?v={{ filemtime(public_path() . '/assets/js/site.js') }}"></script>

@yield('scripts')

@if (env('GA'))
  <!-- Google Analytics -->
  <script>
  window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
  ga('create', '{{ env("GA") }}', 'auto');
  ga('send', 'pageview');
  </script>
  <script async src='https://www.google-analytics.com/analytics.js'></script>
  <!-- End Google Analytics -->
@endif

@if (env('GTM'))
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id={{ env('GTM') }}"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', '{{ env("GTM") }}');
  </script>
@endif
