<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1.0, maximum-scale=1.0">

    <title>@yield('title', setting('site.title') ?: config('site.title'))</title>

    <meta name="description" content="@yield('description', setting('site.description') ?: config('site.description'))">
    <meta name="keywords" content="@yield('keywords', setting('site.keywords') ?: config('site.keywords'))">

    @section('twitter_meta')
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="">
    @show

    @section('og')

    <meta property="og:site_name" content="{{ config('site.name') }}">
    <meta property="og:url" content="{{ URL::current() }}">
    <meta property="og:type" content="website">
    <meta property="og:title" content="@yield('og:title', setting('site.title') ?: config('site.title'))">
    <meta property="og:description" content="@yield('description', setting('site.description') ?: config('site.description'))">
    <meta property="og:image" content="@yield('og:image', config('site.default_image') )">
    @show

    <link rel="canonical" href="{{ URL::current() }}">

    <meta name="base_url" content="{{ URL::to('/') }}">
    <meta name="_token" content="{{ csrf_token() }}">

    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ assets_url('images/favicons/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ assets_url('images/favicons/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ assets_url('images/favicons/site.webmanifest') }}">

    <link rel="stylesheet" href="{{ assets_url('css/vendors/bootstrap/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ assets_url('css/site.css') }}?v={{ filemtime(public_path() . '/assets/css/site.css') }}">

    <link rel="preconnect" href="https://use.fontawesome.com/" crossorigin>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/solid.css" integrity="sha384-v2Tw72dyUXeU3y4aM2Y0tBJQkGfplr39mxZqlTBDUZAb9BGoC40+rdFCG0m10lXk" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/brands.css" integrity="sha384-IiIL1/ODJBRTrDTFk/pW8j0DUI5/z9m1KYsTm/RjZTNV8RHLGZXkUDwgRRbbQ+Jh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/fontawesome.css" integrity="sha384-q3jl8XQu1OpdLgGFvNRnPdj5VIlCvgsDQTQB6owSOHWlAurxul7f+JpUOVdAiJ5P" crossorigin="anonymous">

    @yield('styles')

    <script>
      window.base_url = '{{ URL::to('/') }}';
    </script>
  </head>
  <body>
    @include('_partials.fb-pixel')

    @section('header')
      @include('_partials.header')
    @show

    <div class="page @yield('pageClass', '')">
      @yield('content')
    </div>

    @section('footer')
      @include('_partials.footer')
    @show

    @include('_partials.scripts')

  </body>
</html>
