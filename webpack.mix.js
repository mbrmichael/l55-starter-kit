let mix = require('laravel-mix');
mix.pug = require('laravel-mix-pug');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

let html_path = 'public/html/';

mix.setPublicPath('public/assets/');
mix.disableNotifications();

mix.sass('resources/assets/sass/site.scss', 'css/')
  .sass('resources/assets/sass/docs.scss', 'css/')
  .options({
    processCssUrls: false
  })
  .scripts([
    'resources/assets/js/plugins/*',
    'resources/assets/js/site.js',
  ], 'public/assets/js/site.js')
  .sourceMaps()
  .pug('resources/assets/pug/*.pug', html_path, {
      pug: {
        pretty: true
      }
    })
  ;
