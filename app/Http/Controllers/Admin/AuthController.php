<?php namespace App\Http\Controllers\Admin;

use Auth, Hash;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use App\Http\Requests\Admin\ChangePasswordRequest;

class AuthController extends AdminController
{
    use AuthenticatesUsers;

    protected function redirectTo()
    {
        return route('admin.dashboard');
    }

    public function getLogin()
    {
        return view('admin.auth.login');
    }

    public function postLogin(Request $request)
    {
        // Copied from AuthenticatesUsers@login

        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);

        // Custom

        // $credentials = [
        //     'email' => $request->email,
        //     'password' => $request->password,
        // ];

        // if (Auth::attempt($credentials, $request->remember_me)) {
        //     return redirect()->intended('admin.dashboard');
        // }

        // return redirect()->back()
        //                 ->withInput()
        //                 ->with('error', 'Login Failed.');
    }

    public function getChangePassword()
    {
        return view('admin.auth.change-password');
    }

    public function postChangePassword(ChangePasswordRequest $request)
    {
        $user = Auth::user();
        $user->password = Hash::make($request->password);
        $user->save();

        session()->flash('success', 'Password changed');

        return redirect()->route('admin.dashboard');
    }

    public function getLogout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect()->route('admin.login');
    }
}
