<?php namespace App\Http\Controllers\Admin;

use Image;
use Storage;

use Illuminate\Http\Request;

class SiteController extends AdminController
{
    public function getIndex()
    {
        $totals = [
            'Users' => 100,
        ];

        return view('admin.index', compact('totals'));
    }

    public function postImageUpload(Request $request)
    {
        $allowed = ['png', 'jpg', 'jpeg', 'gif'];
        $dir = 'medias/';

        if ($request->storage) {
            $dir = $request->storage . '/';
        }

        $return = [];

        if ($request->hasFile('file')) {
            $files = $request->file;

            foreach ($files as $key => $file) {
                $extension = strtolower($file->getClientOriginalExtension());
                $filename = str_random(12) . '.' . $extension;

                if (! in_array($extension, $allowed)) {
                    return ['errors' => 'file type is not allowed'];
                }

                $path = $file->storeAs($dir, $filename);

                $filepath = $dir . $filename;

                $return['file-' . $key] = [
                    'url'      => uploads_image($filepath),
                    'thumb'    => uploads_image($filepath, ['fit' => '150x150']),
                    'relative' => $filepath,
                ];
            }

            return $return;
        }

        return [
            'error' => true,
            'message' => 'no files'
        ];
    }

    public function getImageList(Request $request)
    {
        $dir = 'medias';

        if ($request->storage) {
            $dir = $request->storage . '/';
        }

        $files = Storage::files($dir);

        // order by modified time
        $files = collect($files)->sortByDesc(function ($file) {
            return Storage::lastModified($file);
        });

        $images = [];
        foreach ($files as $file) {
            $filepath = $file;
            $filename = ltrim(str_replace($dir, '', $file), '/');

            $images[] = [
                'thumb'    => uploads_image($filepath, ['fit' => '150x150']),
                'url'      => uploads_image($filepath),
                'title'    => $filename,
                'relative' => $filepath,
            ];
        }

        return $images;
    }

    public function getSetLanguage(Request $request)
    {
        // Only used for listing items, eg: 'post/index.blade.php'
        $cookie = cookie('selected_language', $request->lang);

        return redirect()->back()->withCookie($cookie);
    }
}
