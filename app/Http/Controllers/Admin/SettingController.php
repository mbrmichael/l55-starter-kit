<?php namespace App\Http\Controllers\Admin;

use Cache;

use App\Models\Setting;

use App\Http\Requests\Admin\SettingRequest;

class SettingController extends AdminController
{
    const CACHE_KEY = 'settings';

    public function getIndex()
    {
        $settings = Setting::orderBy('key')
                            ->paginate(20);

        $cached = Cache::get(self::CACHE_KEY) ? : [];

        return view('admin.setting.index', compact('cached', 'settings'));
    }

    public function getCreate()
    {
        $setting = new Setting;

        return view('admin.setting.form', compact('setting'));
    }

    public function postCreate(SettingRequest $request)
    {
        $new_setting = [
            'key'         => $request->key,
            'value'       => $request->value,
            'description' => $request->description,
        ];

        Setting::create($new_setting);

        // Rebuild Cache
        Cache::put(self::CACHE_KEY, Setting::get(), 1440);

        session()->flash('success', 'Setting created');

        return redirect()->route('admin.setting');
    }

    public function getEdit(Setting $setting)
    {
        return view('admin.setting.form', compact('setting'));
    }

    public function postEdit(Setting $setting, SettingRequest $request)
    {
        $update_setting = [
            'value' => $request->value,
        ];

        $setting->update($update_setting);

        // Rebuild Cache
        Cache::put(self::CACHE_KEY, Setting::get(), 1440);

        session()->flash('success', 'Setting edited');

        return redirect()->route('admin.setting');
    }

    public function getRebuildCache()
    {
        $settings = Setting::get();

        Cache::put(self::CACHE_KEY, $settings, 1440);

        session()->flash('success', 'Setting cache rebuilt');

        return redirect()->route('admin.setting');
    }
}
