<?php namespace App\Http\Controllers;

use Image;
use Storage;

use Illuminate\Http\Request;

class ImageController extends Controller
{
    public function getImage($url, Request $request)
    {
        $driver = Storage::getDefaultDriver();

        $pathinfo = pathinfo($url);
        $dir = $pathinfo['dirname'];
        $filename = $pathinfo['basename'];
        $extension = !empty($pathinfo['extension']) ? $pathinfo['extension'] : null;

        // Check Extension
        $allowed_extension = ['jpg', 'jpeg', 'png', 'gif'];
        if (!in_array($extension, $allowed_extension)) {
            abort(404);
        }

        // Original file not found
        if (! Storage::exists("{$dir}/{$filename}")) {
            abort(404);
        }

        // Image: Resize
        if ($size = $request->get('resize')) {
            $this->allowedSize('resize', $size);

            $size = explode('x', $size);
            if (count($size) != 2) {
                abort(404);
            }

            $x = intval($size[0]);
            $y = intval($size[1]);

            $filepath = "{$dir}/resize{$x}x{$y}/{$filename}";

            if (! Storage::exists($filepath)) {
                $image = Image::make(Storage::get("$dir/$filename"))
                                ->resize($x, $y, function ($constraint) {
                                    $constraint->aspectRatio();
                                    // $constraint->upsize();
                                })
                                ->encode();
                Storage::put($filepath, $image);
            }

            return response()->file("{$driver}/{$filepath}");
        }

        // Image: Fit
        if ($size = $request->get('fit')) {
            $this->allowedSize('fit', $size);

            $size = explode('x', $size);
            if (count($size) != 2) {
                dd('there');
                abort(404);
            }

            $x = intval($size[0]);
            $y = intval($size[1]);

            $filepath = "{$dir}/fit{$x}x{$y}/{$filename}";

            if (! Storage::exists($filepath)) {
                $image = Image::make(Storage::get("$dir/$filename"))
                                ->fit($x, $y, function ($constraint) {
                                    $constraint->aspectRatio();
                                    // $constraint->upsize();
                                })
                                ->encode();
                Storage::put($filepath, $image);
            }

            return response()->file("{$driver}/{$filepath}");
        }

        // Return original file
        return response()->file("{$driver}/{$dir}/{$filename}");
    }

    private function allowedSize($type, $size)
    {
        if (in_array($size, config('image.allowed_size.*'))) {
            return;
        }

        if (in_array($size, config('image.allowed_size.' . $type))) {
            return;
        }

        abort(404);
    }
}
