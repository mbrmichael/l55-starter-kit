<?php

if (! function_exists('assets_url')) {
    /**
     * Get the URL to an asset
     *
     * @param  string  $path
     * @param  bool    $secure
     * @return string
     */
    function assets_url($asset)
    {
        return asset('assets/' . ltrim($asset, '/'));
    }
}

if (! function_exists('uploads')) {
    /**
     * Generate the URL to uploads path
     *
     * @param  string $upload
     * @return string
     */
    function uploads($upload)
    {
        return Storage::url($upload);
        // return asset('storage/' . ltrim($upload, '/'));
    }
}

if (! function_exists('uploads_path')) {
    /**
     * Generate the path to uploads file
     *
     * @param  string $upload
     * @return string
     */
    function uploads_path($upload)
    {
        return public_path() . '/uploads/' . ltrim($upload, '/');
    }
}

if (! function_exists('setting')) {
    function setting($key)
    {
        $settings = Cache::remember('settings', 1440, function() {
            return App\Models\Setting::all();
        });

        $setting = $settings->where('key', $key)->first();

        return $setting ? $setting->value : null;
    }
}
