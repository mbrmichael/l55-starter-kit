<?php

Route::get('login', ['as' => 'admin.login', 'middleware'=> 'guest', 'uses' => 'AuthController@getLogin']);
Route::post('login', ['as' => 'admin.login.submit', 'uses' => 'AuthController@postLogin']);

Route::group(['middleware' => 'auth'], function() {
    Route::get('/', ['as' => 'admin.dashboard', 'uses' => 'SiteController@getIndex']);

    Route::get('change-password', ['as' => 'admin.change-password', 'uses' => 'AuthController@getChangePassword']);
    Route::post('change-password', ['as' => 'admin.change-password.submit', 'uses' => 'AuthController@postChangePassword']);

    Route::get('logout', ['as' => 'admin.logout', 'uses' => 'AuthController@getLogout']);

    Route::get('setlang', ['as' => 'admin.setlang', 'uses' => 'SiteController@getSetLanguage']);

    // Image upload & list
    Route::post('imageupload', ['as' => 'admin.imageupload', 'uses' => 'SiteController@postImageUpload']);
    Route::get('imagelist', ['as' => 'admin.imagelist', 'uses' => 'SiteController@getImageList']);

    // Setting
    Route::get('setting', ['as' => 'admin.setting', 'uses' => 'SettingController@getIndex']);
    Route::get('setting/create', ['as' => 'admin.setting.create', 'uses' => 'SettingController@getCreate']);
    Route::post('setting/create', ['as' => 'admin.setting.create', 'uses' => 'SettingController@postCreate']);
    Route::get('setting/edit/{setting}', ['as' => 'admin.setting.edit', 'uses' => 'SettingController@getEdit']);
    Route::post('setting/edit/{setting}', ['as' => 'admin.setting.edit', 'uses' => 'SettingController@postEdit']);
    Route::get('setting/rebuildcache', ['as' => 'admin.setting.rebuildcache', 'uses' => 'SettingController@getRebuildCache']);

});
