<?php

Route::get('image/{url}', ['as' => 'image', 'uses' => 'ImageController@getImage'])->where('url', '(.*)');
