require.config({
    shim: {
        'fullcalendar': ['moment', 'jquery'],
    },
    paths: {
        'fullcalendar': 'assets/admin/tabler/plugins/fullcalendar/js/fullcalendar.min',
        'moment': 'assets/admin/tabler/plugins/fullcalendar/js/moment.min',
    }
});
