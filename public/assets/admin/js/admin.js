$(function() {
    let csrf_token = $('meta[name="_token"]').attr('content');
    let image_upload = $('meta[name="image_upload"]').attr('content');
    let image_list = $('meta[name="image_list"]').attr('content');

    // Ajax Setup
    $.ajaxSetup({
      headers: { 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') }
    });

    // Slugify
    $('.slugify').on('change', function() {
      var target = $(this).data('slugify-target');
      var slug = slugify($(this).val());

      if (target) {
        $('#' + target).val(slug);
      }
      else {
        $(this).val(slug);
      }

      function slugify(text) {
        return text
          .toLowerCase()
          .replace(/ /g,'-')
          .replace(/[^\w-]+/g,'');
      }
    });

    // Redactor
    $('.redactor').each(function() {
      let storage = $(this).data('storage') !== undefined ? $(this).data('storage') : '';

      $(this).redactor({
        minHeight: '300px',
        buttons: ['format', 'bold', 'italic', 'underline', 'deleted', 'alignment', 'lists', 'image', 'file', 'horizontalrule', 'link', 'html'],
        plugins: ['alignment', 'video', 'imagemanager'],
        imageUpload: image_upload + '?storage=' + storage + '&_token=' + csrf_token,
        imageManagerJson: image_list + '?storage=' + storage,
      });
    });
});
